import React, { Component } from 'react'
import 'antd/dist/antd.css';
import { Modal, Input , Button, Icon, Table} from 'antd';

import styled from 'styled-components'

import noProfile from '../img/user.svg'


const Search = Input.Search;



class Company extends Component {

  state = {
    ModalText: 'Content of the modal',
    visible: false,
    confirmLoading: false,
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
    console.log('success')
    console.log(this.state.visible)
  }

  handleOk = () => {
    this.setState({
      // ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 1000);
  }
  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  }

    render() {
      const { visible, confirmLoading} = this.state;

      
      
        return (
            <Wrapper>
            <div>
            <Search placeholder="input search text" enterButton="Search" size="large" className="search-btt" />
            <Button type="primary" size="large" className="add-btt" onClick={this.showModal}><Icon type="plus" /></Button>
            <Modal title="Add Collection"
              visible={visible}
              onOk={this.handleOk}
              confirmLoading={confirmLoading}
              onCancel={this.handleCancel} 
              className="modal"            
            >            
              <img src={noProfile} alt="no-profile" className="profile" style={{width: '100px'}} />
              <p >Keyword: <Input placeholder="Basic usage" style={{float: 'left', width: 200}}/></p>
              <span>Firstname: </span><Input placeholder="Basic usage" />
              <span>Lastname: </span><Input placeholder="Basic usage" />
              <span>E-mail: </span><Input placeholder="Basic usage" />
              <span>Company: </span><Input placeholder="Basic usage" />
            
            </Modal>

            </div>
            <Table columns={columns} dataSource={data} className="table" />
            
            
            <div>      
            
            </div>
            
            </Wrapper>
        )
    }
}

const columns = [
  {
    title: 'no.',
    dataIndex: 'no',
    key: 'no'
  },
  {
    title: 'FB_id',
    dataIndex: 'FB_id',
    key: 'FB_id'
  },  
  {
    title: 'FB_name',
    dataIndex: 'FB_name',
    key: 'FB_name',
    render: text => <a href="#">{text}</a>,
  },
  {
    title: 'Keyword',
    dataIndex: 'keyword',
    key: 'keyword'
  },
  {
    title: 'E-mail',
    dataIndex: 'e-mail'
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  }, {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  }, 
  {
    title: 'Detail',
    key: 'action',
    render: (text, record) => (
      <span>       
        <a className="ant-dropdown-link" onClick={this.showModal}>
          More detail <Icon type="down" />
        </a>
      </span>
    ),
  }
];
  
  const data = [{
    key: '1',
    no: 1,
    FB_name: 'BuzzFreeze',
    FB_id: '794352360601947',
    keyword: 'Python',
    age: 32,
    address: 'New York No. 1 Lake Park',
  }, {
    key: '2',
    no: 2,
    FB_name: 'BuzzFreeze',
    FB_id: '794352360601947',
    keyword: 'C++',
    age: 42,
    address: 'London No. 1 Lake Park',
  }, {
    key: '3',
    no: 3,
    FB_name: 'BuzzFreeze',
    FB_id: '794352360601947',
    keyword: 'JAVA',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
  },
  {
    key: '4',
    no: 4,
    FB_name: 'BuzzFreeze',
    FB_id: '794352360601947',
    keyword: 'Nodejs',
    age: 32,
    address: 'Shellsea',
  },
  {
    key: '5',
    no: 5,
    FB_name: 'BuzzFreeze',
    FB_id: '794352360601947',
    keyword: 'Javascript',
    age: 32,
    address: 'Manchester is blue',
  },
  {
    key: '6',
    no: 6,
    FB_name: 'BuzzFreeze',
    FB_id: '794352360601947',
    keyword: 'C#',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
  }

];


const Wrapper = styled.section`
  padding: 1em;
  background: white;
  height: 500px;
  .search-btt {
      width: 300px;
      float: right;
      margin-left: 20px;
  }
  .add-btt{
      float: right;
      
  }
  .card-board{      
      margin: 0 auto;      
      margin-top: 30px;
      margin-left: 50px;      
      width: 900px;
      height: 100px;
      float: left;
  }
  .table{
      float: right;
      margin-top: 20px;
      width: 980px;
     
  }

  .modal {
    color:red;
    span {
      color: red;
    }

  }
  
 
  
`





export default Company




