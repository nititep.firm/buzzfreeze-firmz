import React , { Component } from 'react'
import {Link} from 'react-router-dom'
import "antd/dist/antd.css";

import styled from 'styled-components'
import logo from '../img/logo.jpg'

const HeadBar = styled.div`
  background: white;
  width: 300px;
  height: 100px;  
  float: left;
  .logo {
    padding-top: 25px;
  }
`
const HeadNav = styled.div`
  background: #1a61ad;
  height: 100px;
  h1 {
    color: white;
    text-align: center;
    padding: 15px;
    font-size: 40px;     
  }  
  
  
`


class Header extends Component {
    render(){
        return (
        <div>
            <HeadBar><Link to="/"><img src={logo} className="logo" alt="logo"/></Link></HeadBar>
            <HeadNav><h1>DASHBOARD</h1></HeadNav>

        </div>)
    }
}

export default Header