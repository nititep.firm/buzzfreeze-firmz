import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom'

import 'antd/dist/antd.css';

import { Menu, Icon } from 'antd';
import styled from 'styled-components'

import User from './components/User'
import Home from './components/Home'
import Headder from './components/Header'
import Sidebar from './components/Sidebar'
import Company from './components/Company'
import ListCollection from './components/ListCollection'


class App extends Component {


  
  render() {
    return (
      <BrowserRouter>
          <Wrapper>
            <Headder/>
            <Sidebar/>
                <WorkFlow>
                    <Route exact path="/" component={ListCollection}/>
                    <Route path="/user" component={User}/>
                    <Route path="/company" component={Company}/> 
                </WorkFlow>   
          </Wrapper>
      </BrowserRouter>
    );
  }
}

const Wrapper = styled.section`
  padding: 1em;
  background: lightblue;
  height: 820px;
`;


const WorkFlow = styled.div`
  background: white;
  height: 625px;
`

export default App;
